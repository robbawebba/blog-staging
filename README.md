# robbawebba.gitlab.io
The source code for my blog at robbawebba.gitlab.io

## Installation and Running
1. Download and install [Hugo](https://gohugo.io/getting-started/installing/), a powerful & fast static site framework/generator written in Go
2. Clone this repository: `git clone https://gitlab.com/robbawebba/robbawebba.gitlab.io.git && cd robbawebba.gitlab.io`
3. run the Hugo dev server: `hugo -D server`.
	a. The -D flag tells Hugo to build any drafts or unpublished posts. See `hugo -h` for info on the CLI commands and options

