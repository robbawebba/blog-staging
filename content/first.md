---
title: "First"
date: 2018-04-22T23:38:42-07:00
draft: true
author: "Rob Weber"
---

how can I begin to `explain` my love for Hugo right now?

```go
import fmt

func main() {
	fmt.Println("hello!")
}
```
